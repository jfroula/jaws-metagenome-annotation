workflow jgi_metagenome_annot {
    File contigs
    File data
    String reads 
    
    call prodigal {
        input: contigs=contigs
    }
    call hmmer {
        input: inproteins=prodigal.faa
    }
    call cmsearch {
        input: contigs=contigs,
            data=data
    }
    call get_read_cov_stats {
        input:  reads=reads,
            contigs=contigs
    }
}

task prodigal {
    File contigs

    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.4 prodigal -c -n -f gff -p meta -q \
              -i ${contigs} -o Prodigal.gff \
              -a Prodigal_proteins.faa -d Prodigal_genes.fna \
              -s Prodigal_starts.txt
    }
    output {
        File faa = "Prodigal_proteins.faa"
        File fna = "Prodigal_genes.fna"
        File gff = "Prodigal.gff"
        File starts = "Prodigal_starts.txt"
    }
    runtime {
        cpu: threads 
        time: "12:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "prodigal"
        node: 1
        nwpn: 1
    }
}

task hmmer {
    File inproteins
    Int threads=4

    command {
        shifter --image=jfroula/interproscan:1.0.1 interproscan.sh -i ${inproteins} --appl pfam,tigrfam,superfamily -f gff3 --iprlookup --goterms --outfile pfam.gff3
    }
    output {
        File pfam_gff = "pfam.gff3"
    }
    runtime { 
        cpu: threads 
        time: "12:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "prodigal"
        node: 1
        nwpn: 1
    }

}

task cmsearch {
    File contigs
    File data
    Int threads = 4

    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.4 cmsearch --cpu ${threads} -o /dev/null --tblout Rfam.tbl --notextw \
            --cut_tc ${data}/Rfam.cm ${contigs} 

        shifter --image=jfroula/jgi_meta_annot:1.1.4 create_gff.py Rfam.tbl Infernal > Rfam.gff # | shifter --image=jfroula/jgi_meta_annot:1.1.4 filter_clans -c ${data}/Rfam_name_to_clan.tsv > Rfam.gff
    }
    output {
        File rfam_tbl = "Rfam.tbl"
        File rfam_gff = "Rfam.gff"
    }
    runtime {
        cpu: threads 
        time: "48:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "cmsearch"
        node: 1
        nwpn: 1
    }
}

task get_read_cov_stats {
    File reads
    File contigs

    runtime {
        cpu: threads 
        time: "12:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "prodigal"
        node: 1
        nwpn: 1
    }
    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.4 read_cov_stats.sh ${reads} ${contigs}
    }
    output {
        File contig_cov = "contigcoverage.tsv"
        File contigstats = "contigstats.txt"
        File coverage = "coverage.tsv"
        File mapped_bam = "mappedreads.bam"
        File mapped_bai = "mappedreads.bam.bai"
        File mapped_stats = "mappingstats.txt"
    }
}

