FROM continuumio/miniconda
RUN conda config --add channels defaults && \
    conda config --add channels bioconda && \
    conda config --add channels conda-forge

RUN conda install -y prodigal==2.6.3
RUN conda install -y hmmer==3.2.1
RUN conda install -y infernal==1.1.2
RUN conda install -y ruby==2.4.5

RUN conda install -y -c bioconda seqtk==1.3
RUN conda install -y -c bioconda bwa==0.7.17
RUN conda install -y -c bioconda samtools==1.9
COPY bin/ /usr/local/bin
