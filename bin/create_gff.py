#!/usr/bin/env python
import os,sys
if (len(sys.argv) < 3):
    print("Usage: {} <table from hmmscan or cmsearch> <tool[HMMER|Infernal]>".format(sys.argv[0]))
    sys.exit()

infile=sys.argv[1]
tool=sys.argv[2]


def hmmer_gff3(infile):
    print("##gff-version 2")
    print("##seqid\tsource\tmodel_id\tstart\tend\tscore\tstrand\tphase\tattributes")
    with open(infile,"r") as intable:
        for line in intable:
            if line.startswith('#'):
                continue
            s = line.split()
            tags = s[29] + ";Name={}".format(s[3])
            print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(s[0],tool,s[4],s[17],s[18],s[13],".",".",tags))

def cmscan_gff2(infile):
    print("##gff-version 2")
    print("##seqid\tsource\tmodel_id\tstart\tend\tscore\tstrand\tphase\tattributes")
    with open(infile,"r") as intable:
        for line in intable:
            if line.startswith('#'):
                continue

            s = line.split()
            if (int(s[7]) <= int(s[8])):
                start=s[7]
                end=s[8]
            else:
                start=s[8]
                end=s[7]

            tags = "trunk={};Name={};Desc={}".format(s[10],s[0],' '.join(s[17:]))
            print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(s[2],tool,s[4],start,end,s[14],".",".",tags))


if tool == "HMMER":
    hmmer_gff3(infile)
elif tool == "Infernal":
    cmscan_gff2(infile)
else:
    print("Please use HMMER or Infernal for name of tool (e.g. third argument).")
    sys.exit()
