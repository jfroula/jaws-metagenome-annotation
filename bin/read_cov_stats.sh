#!/usr/bin/env bash
set -ex

READS=$1
CONTIGS=$2
if [[ ! $READS ]] || [[ ! $CONTIGS ]]; then
    echo "Usage: $0 <reads> <contigs>"
    exit 1
fi
if [[ ! -s $READS ]]; then 
    echo "Reads file missing or empty: $READS"
    exit 1
fi
if [[ ! -s $CONTIGS ]]; then 
    echo "Contigs file missing or empty: $CONTIGS"
    exit 1
fi

ROOT_CONTIGS=${CONTIGS##*/}
if [[ ! -e $ROOT_CONTIGS ]]; then
    ln -s $CONTIGS $ROOT_CONTIGS
fi
samtools faidx $ROOT_CONTIGS 
bwa index $ROOT_CONTIGS
bwa mem -t 10 -p $ROOT_CONTIGS $READS | samtools view -b - | samtools sort -l 9 -m 6G -@ 10 -o mappedreads.bam -

samtools index mappedreads.bam
samtools depth mappedreads.bam > coverage.tsv
stats $CONTIGS > contigstats.txt
samtools flagstat mappedreads.bam > mappingstats.txt
samtools_depth_coverage coverage.tsv > contigcoverage.tsv
