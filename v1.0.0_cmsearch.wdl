workflow cmsearch_wf {
    File contigs
    File data

    call shard {
        input: contigs=contigs
    }

    Array[String] shard_array = shard.shards

    scatter(coords in shard_array) {
        call cmsearch {
            input: contigs=contigs,
               data=data,
               coords = coords
        }
    }
}

task shard {
    File contigs
    String bname = basename(contigs)

    command {
        set -x
        conda activate jaws && \
        fasta_indexer.py --input ${contigs} --output ${bname}.index 

        conda activate jaws && \
        create_tasks.py -if ${bname}.index -ff ${contigs} -of ${bname}.sharded -bs 10000

        conda deactivate
    }

    output {
        Array[String] shards = read_lines("${bname}.sharded")
    }
}

task cmsearch {
    File contigs
    File data
    String coords
    Int threads = 16

    command<<< 
        set -x
        # parse coords
        start=$(echo ${coords} | awk '{print $1}') 
        end=$(echo ${coords} | awk '{print $2}')

        tmpfile=`mktemp`
        conda activate jaws && \
        shard_reader.py -i ${contigs} -s $start -e $end > $tmpfile

        shifter --image=jfroula/jgi_meta_annot:1.1.4 cmsearch --cpu ${threads} -o /dev/null --tblout Rfam.tbl --notextw \
            --cut_tc ${data}/Rfam.cm $tmpfile

        shifter --image=jfroula/jgi_meta_annot:1.1.4 create_gff.py Rfam.tbl CMSEARCH > Rfam.gff # | shifter --image=jfroula/jgi_meta_annot:1.1.4 filter_clans -c ${data}/Rfam_name_to_clan.tsv > Rfam.gff
    >>>
    output {
#        String out = read_string(stdout())
        File rfam_tbl = "Rfam.tbl"
        File rfam_gff = "Rfam.gff"
    }
    runtime {
        cpu: threads
        time: "2:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "haswell_nodes"
        node: 1
        nwpn: 2
    }
}
