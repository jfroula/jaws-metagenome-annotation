#!/bin/bash
set -ex
#prodigal.json
#prodigal.wdl
memtime java -jar /global/dna/projectdirs/DSI/workflows/cromwell/cromwell.jar run prodigal.wdl -i prodigal.json

#hmmer.json
#hmmer.wdl
memtime java -jar /global/dna/projectdirs/DSI/workflows/cromwell/cromwell.jar run hmmer.wdl -i hmmer.json

#cmsearch.json
#cmsearch.wdl
memtime java -jar /global/dna/projectdirs/DSI/workflows/cromwell/cromwell.jar run cmsearch.wdl -i cmsearch.json

#read_cov_stats.json
#read_cov_stats.wdl
memtime java -jar /global/dna/projectdirs/DSI/workflows/cromwell/cromwell.jar run read_cov_stats.wdl -i read_cov_stats.json


