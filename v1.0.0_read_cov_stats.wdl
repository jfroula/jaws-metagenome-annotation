workflow read_cov_stats_wf {
    File contigs
    String reads 
    
    call get_read_cov_stats {
        input:  reads=reads,
            contigs=contigs
    }
}

task get_read_cov_stats {
    File reads
    File contigs

    runtime {
        cluster: "cori"
        poolname: "medium"
    }
    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.3 read_cov_stats.sh ${reads} ${contigs}
    }
    output {
        File contig_cov = "contigcoverage.tsv"
        File contigstats = "contigstats.txt"
        File coverage = "coverage.tsv"
        File mapped_bam = "mappedreads.bam"
        File mapped_bai = "mappedreads.bam.bai"
        File mapped_stats = "mappingstats.txt"
    }
}

