workflow cmscan_wf {
    File contigs
    File data

    call shard {
        input: contigs=contigs
    }

    Array[String] shard_array = shard.shards

    scatter(coords in shard_array) {
        call cmscan {
            input: contigs=contigs,
               data=data,
               coords = coords
        }
    }

    call mergeRfam {
        input: rfam_gff = cmscan.rfam_gff
    }
}

task shard {
    File contigs
    String bname = basename(contigs)

    command {
        set -x
        shifter --image=jfroula/jgi_meta_annot:1.1.6 fasta_indexer.py --input ${contigs} --output ${bname}.index 

        shifter --image=jfroula/jgi_meta_annot:1.1.6 create_tasks.py -if ${bname}.index -ff ${contigs} -of ${bname}.sharded -bs 10000
    }

    output {
        Array[String] shards = read_lines("${bname}.sharded")
    }
}

task cmscan {
    File contigs
    File data
    String coords
    Int threads = 16

    command<<< 
        set -x
        # parse coords
        start=$(echo ${coords} | awk '{print $1}') 
        end=$(echo ${coords} | awk '{print $2}')

        shifter --image=jfroula/jgi_meta_annot:1.1.6 shard_reader.py -i ${contigs} -s $start -e $end | \
        shifter --image=jfroula/jgi_meta_annot:1.1.6 cmscan --cpu ${threads} --noali -o /dev/null \
                --tblout Rfam.tbl --notextw --cut_tc ${data}/Rfam.cm -

        shifter --image=jfroula/jgi_meta_annot:1.1.6 create_gff.py Rfam.tbl Infernal > Rfam.gff 
    >>>
    output {
#        File rfam_tbl = "Rfam.tbl"
        File rfam_gff = "Rfam.gff"
    }
    runtime {
        cpu: 16
        time: "2:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "haswell_nodes"
        node: 1
        nwpn: 2
    }
}

task mergeRfam {
#    Array[File] rfam_tbl
    Array[File] rfam_gff

    command {
        # Rfam.gff merge
        cat ${sep=' ' rfam_gff} | head -2 > Rfam_merged.gff
        cat ${sep=' ' rfam_gff} | sed '/^##/d' | sort -k1,1d -k4,4n >> Rfam_merged.gff

#        # Rfam.tbl merge
#        cat ${rfam_tbl[0]} | head -2 > Rfam_merged.tbl
#        cat ${sep=' ' rfam_tbl} | sed '/^#/d' | sort -k7,7n >> Rfam_merged.tbl
#        echo "#" >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Program' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Version' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Pipeline' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Query' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Target' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Option' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Current' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Current' >> Rfam_merged.tbl
#        cat ${rfam_tbl[0]} | grep '# Date' >> Rfam_merged.tbl
#        echo "# [ok]" >> Rfam_merged.tbl
    }

    output {
        File merged_rfam_gff = "Rfam_merged.gff"
#        File merged_rfam_tbl = "Rfam_merged.tbl"
    }
}
