workflow hmmer_wf {
    File proteins

    call hmmer {
        input: proteins=proteins
    }
}

task hmmer {
    File proteins
    Int threads=1

    command {
        shifter --image=jfroula/interproscan:1.0.1 interproscan.sh -i ${proteins} --appl pfam,tigrfam,superfamily -f gff3 --iprlookup --goterms --outfile pfam.gff3
    }
    output {
        File pfam_gff = "pfam.gff3"
    }
    runtime { 
        cpu: threads
        time: "2:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "haswell_nodes"
        node: 1
        nwpn: 1
    }

}
