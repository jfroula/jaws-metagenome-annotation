workflow hmmer_wf {
    File proteins
    File data

    call hmmer {
        input: proteins=proteins,
               data=data
    }
}

task hmmer {
    File proteins
    File data
    Int threads=1

    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.3 hmmsearch -o /dev/null --cpu ${threads} --domtblout Prodigal_Pfam.tbl \
            --notextw --cut_tc ${data}/Pfam-A.hmm ${proteins} 

        shifter --image=jfroula/jgi_meta_annot:1.1.3 create_gff.py Prodigal_Pfam.tbl HMMER > Prodigal_Pfam.gff # | shifter --image=jfroula/jgi_meta_annot:1.1.3 filter_clans -c ${data}/Pfam_name_to_clan.tsv > Prodigal_Pfam.gff
    }
    output {
        File pfam_tbl = "Prodigal_Pfam.tbl"
        File pfam_gff = "Prodigal_Pfam.gff"
    }
    runtime { 
        cpu: threads
        time: "2:00:00"
        mem: "115G"
        cluster: "cori"
        poolname:  "haswell_nodes"
        node: 1
        nwpn: 1
    }

}
