# JGI Metagenome Annotation Pipeline (lite)

## Summary
Adapted from Torben's jgi-metagenome-assembly(lite) protocol.

## Running workflow in Cromwell/JAWS
You can run the hmmer, cmsearch or read-coverage steps separately or all together.
To run all together you need to specify the inputs.json as follows:

```
{
    "jgi_metagenome_annot.data": "/global/dna/shared/data/jfroula/jaws_meta_annot_refDbs",
    "jgi_metagenome_annot.cmd_flag": "readcov",
    "jgi_metagenome_annot.reads": "/global/cscratch1/sd/jfroula/jaws-metagenome-annotation/sample.fastq",
    "jgi_metagenome_annot.contigs": "/global/cscratch1/sd/jfroula/jaws-metagenome-annotation/spades3/scaffolds.fasta",
    "jgi_metagenome_annot.cmsearch.threads": 4
}
```

## This is essentially the whole pipeline

#### predict proteins
```
prodigal -c -n -f gff -p meta -q \
              -i $CONTIGS -o Prodigal.gff \
              -a Prodigal_proteins.faa -d Prodigal_genes.fna \
              -s Prodigal_starts.txt 
```

#### find pfams
```
hmmsearch -o /dev/null --cpu $THREADS --domtblout Prodigal_Pfam.tbl \
    --notextw --cut_tc $DATA/Pfam-A.hmm Prodigal_proteins.faa 

hmmer_tbl_gff Prodigal_Pfam.tbl | filter_clans \
    -c $DATA/Pfam_name_to_clan.tsv > Prodigal_Pfam.gff
```


#### find rRNAs & tRNAs
```
cmsearch --cpu $THREADS -o /dev/null --tblout Rfam.tbl --notextw \
    --cut_tc $DATA/Rfam.cm $CONTIGS 

infernal_tbl_gff < Rfam.tbl | filter_clans -c $DATA/Rfam_name_to_clan.tsv > Rfam.gff
```

## Create databases
```
mkdir -p /data
cd /data

# download Pfams and Rfams
wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz && gunzip Pfam-A.hmm.gz
wget ftp://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.cm.gz && gunzip Rfam.cm.gz

# download clan to pfam or rfam tables and format them
wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/clan.txt.gz --output-document=pfam_clan.txt.gz && gunzip pfam_clan.txt.gz
awk '{print $2"\t"$1}' pfam_clan.txt > Pfam_name_to_clan.tsv
rm pfam_clan.txt

wget ftp://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/database_files/clan.txt.gz --output-document=rfam_clan.txt.gz && gunzip rfam_clan.txt.gz
awk '{print $2"\t"$1}' rfam_clan.txt > Rfam_name_to_clan.tsv
rm rfam_clan.txt
```

# Dependency Graph

![graph](workflow_20190503.png)
