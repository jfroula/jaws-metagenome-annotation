workflow prodigal_wf {
    File contigs
    File? data
    String? reads 
    String? cmd_flag="all"
    
    call prodigal {
        input: contigs=contigs
    }
}

task prodigal {
    File contigs

    command {
        shifter --image=jfroula/jgi_meta_annot:1.1.3 prodigal -c -n -f gff -p meta -q \
              -i ${contigs} -o Prodigal.gff \
              -a Prodigal_proteins.faa -d Prodigal_genes.fna \
              -s Prodigal_starts.txt
    }
    output {
        File faa = "Prodigal_proteins.faa"
        File fna = "Prodigal_genes.fna"
        File gff = "Prodigal.gff"
        File starts = "Prodigal_starts.txt"
    }
    runtime {
        cluster: "cori"
        poolname: "small"
    }
}
